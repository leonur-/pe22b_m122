# mal schauen, ob ob ich aus Verzeichnissen, Dateien lesen kann

$verzeichnis_namensdateien = "_namensdateien"

# Da PowerShell objektorientiert ist, kann man gleich das 
# Name-Attribut aus der Resultatsliste erfragen.
# Dies wird sogleich in eine Variable gespeichert.
$allFiles = (dir $verzeichnis_namensdateien).name 

# Ich versuche mal alles aufzulisten
# Wir haben jetzt ja in "allFiles" die ganze Liste der Files
# im Verzeichnis gespeichert.
# mit "type" kann ich nun auslesen, was in jeder Datei drin ist.
# Wenn man in PowerShell ein 2echo" auf eine Liste macht, 
# kann er die ganze Liste ausdrucken
foreach ($file in $allFiles) {
    $fileinhalt = type $verzeichnis_namensdateien/$file
    echo $file
    echo "---"
    echo $fileinhalt
    echo "===="
    echo ""
}

# Skript anhalten, um zu sehen, obs Fehler gab
pause