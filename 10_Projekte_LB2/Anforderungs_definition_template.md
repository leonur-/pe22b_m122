# Projekttitel

## Team
- Hans Muster
- Peter Meier

## Anforderungsdefinition

### Kundennutzen:

Mit dem Skript sollen  ..... *In ganzen Sätzen*

### Umgebung und Automation

Das Skript läuft auf ....

Das Skript verarbeitet .... *In ganzen Sätzen*

*Mögliche Details (Beispiele)*:
•	Konfiguration (.cfg): ...
•	Get-Prozedur (.raw): ...
•	Verarbeitung (process): ...
•	Weiterreichung (.fmt): ...
•	Sicherheitsaspekte: ...

*Vielleicht eine Skizze / Mockup*

Copy [Miro-board](https://miro.com/app/board/uXjVOtiO480=/?share_link_id=718458354155)  und passt es an oder erzeugt ein Draw-IO für euer System.

*Erkenntnisse aus der Machbarkeitsabklärung in Bash (oder Python):*
Folgende Features sind vorab untersucht worden und .....

### Muss-Kriterien

Folgende Features sollen implementiert werden, um einen produktiven Ablauf sicherzustellen: 

- ...
- ...

### Kann-Kriterien

Folgende Features können zusätzlich implementiert werden: 

- ...
- ...

