# M122 - Aufgabe

2023-03 MUH


## System-Leistung abfragen

Für eine automatische Systemüberwachung von Servern 
und/oder auch Clients sollen Sie "regelmässig" (cron)
eine Serie von Leistungs-Daten ausgeben.

### Aufgabenstellung


Verwenden dafür Bash- oder Shell-Scripting, um das Script 
zu erstellen und sicherzustellen, dass es auf Ihrem System 
ausgeführt werden kann.

**1.) Formattierte Inhalte**

Formattieren Sie alles in eine gut leserliche Form.

Folgendes ist auszugeben:

- 1. Der Hostname des Systems
- 2. Die Betriebssystemversion
- 3. Der Modellname der CPU
- 4. Die Anzahl der CPU-Cores
- 5. Der gesamte und der genutze Arbeitsspeicher
- 6. Die Menge des verfügbaren Speichers
- 7. Die Menge des freien Speichers
- 8. Die Gesamtgröße des Dateisystems
- 9. Die Menge des belegten Speichers auf dem Dateisystem
- 10. Die Menge des freien Speichers auf dem Dateisystem
- 11. Die aktuelle Systemlaufzeit
- 12. Die aktuelle Systemzeit


**2.) Dateiausgabe wahlweise**

Gefordert ist die Ausgabe **wahlweise** direkt auf das 
Terminal, bzw. die Console, wie auch in eine Datei.

Wenn man **keine Option** angibt: Nur die Terminal-Ausgabe.

Wenn man die Option `-f` angibt, soll zusätzlich die 
Datei **[timestamp]-sys-[hostname].info** erzeugt werden.

Tipp: Benutzen Sie für den Timestamp `date '+%Y-%m-%d_%H%M'`
und für den Hostnamen `hostname` oder `uname -u`

**3.) Regelmässigkeit**

Binden Sie Ihr Skript in die `crontab` ein
und wählen Sie einen geeigneten Ausführungs-Takt.


### Resultat

Ihr Resultat könnte so aussehen:

![./hostinformation.jpg](./hostinformation.jpg)


<hr>

## Bewertung

| Punkte | Beschreibung | 
|-------|--------------|
|     3 | Alle oben genannten Sytem-Infos   |
|     1 | Ausgabe in Datei mit "Switch" und richtigem Dateiname | 
|     1 | Regelmässige Ausführung (Abgabe von 3 Files, die das System im Takt erstellt hat)   |
|     1 | Bonuspunkt für "gute/schöne" Darstellung   |
| **6** | **Total** | 


### Noten

| Note| Punkte    |
|-----|-----------|
| 6.0 |  6  |
| 5.0 |  5  | 
| 4.0 |  4  |
| 1.0 |Nichtabgabe|
