# AWS Public Cloud

Mit der vorliegenden Anleitung erstellen Sie eine Ubuntu-Instanz in der Cloud mit dem Vorteil, dass Sie keine lokalen Ressource verwenden müssen. 

[TOC]

## Installation

Die Lehrperson hat Sie bereits zu einem Amazon/AWS Kurs eingeladen. Sie bekommen die Einladung via E-Mail.

Login-Seite: [Log In to Canvas (instructure.com)](https://awsacademy.instructure.com/login/canvas)

Wählen Sie nun den Kurs "**Lerner Lab**" aus:

![steps_01](./x_gitressources/steps_01.png)


Auf der nächsten Seite wählen Sie den Navigations-Punkte "**Modules**", dann "**Lerner Lab**"

![steps_02](./x_gitressources/steps_02.png)


Klicken Sie nun auf "**Start Lab**". Beim ersten Mal dauert dies ein paar Minuten, weil der Account provisioniert werden muss. sobald der grün Punkte links grün ist, klicken Sie auf den **AWS**-Link (neben dem grünen Punkte).

![steps_03](./x_gitressources/steps_03.png)


Scrollen Sie runter bis Sie die Links im nächsten Screenshot sehen. Starten Sie eine virtuelle Maschine.

![steps_04](./x_gitressources/steps_04.png)

**Es gibt viele Option zur Konfiguration einer VM. Setzen Sie nur die Optionen wie sie auf den folgenden Bildern gezeigt wird.**

Geben Sie ihrer virutellen Instanz einen Namen. 

![steps_05](./x_gitressources/steps_05.png)

Wählen sie die **Ubuntu** Distribution aus als Betriebsystem und dann die Version **22.04**.

![steps_06](./x_gitressources/steps_06.png)

Als Instanz-Typ können Sie "**t2.medium**" auswählen. Seien Nehmen Sie keine grössere Instanz, da Ihre Ressourcen in diesem Schulungsaccount limitiert sind. 

![steps_07](./x_gitressources/steps_07.png)

Verwenden Sie "**vockey**" als Schlüsselpaar. 

![steps_08](./x_gitressources/steps_08.png)


Vergrössern Sie die Disk auf 30GB!

![steps_09](./x_gitressources/steps_09.png)



Öffnen Sie nun den Bereich "**Advanced  details**" und scrollen Sie ganz nach unten zu dem Feld "**User data**". Kopieren Sie hier den [Inhalt dieser Datei](./awsinit.yaml) rein. Dieses Script erstellt die notwendigen Benutzer.

![steps_10](./x_gitressources/steps_10.png)



## Zugang mit Console

Gehen Sie nun zu Ihren Instanzen und wählen Sie ihre erstelle Instanz aus. In den Details können Sie nun die IP kopieren.

**ACHTUNG**: Diese IP wechselt jedesmal, wenn Sie die Instanz neu starten.

![access_01](./x_gitressources/access_01.png)

Mit der IP können Sie nun auf Ihre virtuelle Instanz zugreifen mit dem folgenden Befehl in CMD oder Powershell.

~~~bash
ssh ubuntu@54.145.177.144 -o ServerAliveInterval=30
~~~

Sie müssen natürlich Ihre IP korrekt eingeben. Das **Passwort** finden Sie in der Konfigurations-Datei, die Sie vorher kopierten.

Nun müssen Sie den **fingerprint aktzeptieren** indem Sie "yes" schreiben. Dies machen Sie jedesmal, wenn die IP ändert. Nach erfolgreichem Einloggen, sollten Sie ein Bild wie das folgende vorfinden.

![access_02](./x_gitressources/access_02.png)



## Zugang mit WinScp

Falls Sie Dateien manuell hochladen möchten, können Sie dies via [WinScp](https://winscp.net/eng/download.php) tun. 

Erstellen Sie eine neue "site" und geben Sie die IP ihrer virtuellen Instanz ein. Verwenden Sie "ubuntu" as Benutzername. Verwenden Sie das korrekte Passwort.

![access_04](./x_gitressources/access_03.png)



Ändern Sie dann die SFTP server settings unter "**advanced**". Dies erlaubt Ihnen den Zugriff als "sudo" also mit vollen Admin-Rechten.

![access_05](./x_gitressources/access_05.png)
