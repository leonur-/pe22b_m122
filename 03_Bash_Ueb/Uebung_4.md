## Übung 4 - grep, cut (, awk)::

**a)** Erzeugen Sie eine Textdatei mit folgendem Inhalt: 

```
alpha1:1alpha1:alp1ha
beta2:2beta:be2ta
gamma3:3gamma:gam3ma
obelix:belixo:xobeli
asterix:sterixa:xasteri
idefix:defixi:ixidef 
```


Durchsuchen Sie die Datei mit `grep` nach folgenden Mustern (benutzen Sie die
Option `--color=auto`):

- Alle Zeilen, welche `obelix` enthalten
- Alle Zeilen, welche `2` enthalten
- Alle Zeilen, welche ein `e` enthalten
- Alle Zeilen, welche **nicht** `gamma` enthalten
- Alle Zeilen, welche `1`, `2` oder `3` enthalten (benutzen Sie `-E` und eine regex)

**b)**  Gehen Sie von derselben Datei aus wie in Übung a). Benutzen Sie `cut` und
formulieren Sie damit einen Befehl, um nur folgende Begriffe anzuzeigen:

- Alle Begriffe vor dem ersten :-Zeichen
- Alle Begriffe zwischen den beiden :-Zeichen
- Alle Begriffe rechts des letzten :-Zeichen

**c)**  ***Nur für Knobbler***: Gehen Sie wieder von derselben Datei aus wie in Übung a). Benutzen Sie `awk` und
formulieren Sie damit einen Befehl, um nur die Begriffe anzuzeigen, die sich zwischen dem letzten und dem zweitletzten :-Zeichen befinden. Sie kriegen das gleiche Resultat wie bei der zweiten Übung unter **b)**, aber nun dynamisch und die Anzahl Separatoren spielt keine Rolle mehr.
