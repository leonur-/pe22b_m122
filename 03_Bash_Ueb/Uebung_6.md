## Übung 6 - stdout, stdin, stderr:

**a)** Erzeugen Sie eine Textdatei mit folgendem Inhalt:

```
a
b
c
d
e
```

Benutzen Sie zur Erzeugung `<<` indem Sie Zeile fur Zeile an `cat` übergeben, die
Ausgabe wird in eine Datei umgeleitet. Benutzen Sie das Schlusswort `END`.



**b)** Die Ausführung von `ls -z` erzeugt einen Fehler (da es die Option `-z` nicht gibt).
Starten Sie ls mit -z und leiten Sie die Fehler in eine Datei `/root/errorsLs.log` um.

**c)** Erzeugen Sie eine kl. Textdatei und füllen Sie diese mit Inhalt. Geben Sie die
Textdatei mit cat aus und leiten Sie die Ausgabe wieder in eine neue Datei um.
Benutzen Sie einmal `>` und einmal `>>` (mehrmals hintereinander). Untersuchen Sie
die beiden Situationen, indem Sie jedesmal den Inhalt der Datei wieder ausgeben.
Was pasSiert wenn Sie in dieselbe Datei umleiten wollen?

**d)** Leiten Sie die Ausgabe von whoami in die Datei `info.txt` um

**e)** Hängen Sie die Ausgabe von `id` an die Datei `info.txt` an

**f)** Leiten Sie die Datei `info.txt` als Eingabe an das Programm `wc` um und zählen
Sie damit die Wörter (`-w`)



